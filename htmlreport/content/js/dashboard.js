/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7857142857142857, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "65/ AA go to login page"], "isController": false}, {"data": [1.0, 500, 1500, "AA Click Asset Management"], "isController": true}, {"data": [0.0, 500, 1500, "Submit Risk"], "isController": true}, {"data": [1.0, 500, 1500, "82 / SR  logout"], "isController": false}, {"data": [1.0, 500, 1500, "65/ SR go to login page"], "isController": false}, {"data": [1.0, 500, 1500, "76 SR open risk management"], "isController": false}, {"data": [0.0, 500, 1500, "73/ SR login"], "isController": false}, {"data": [1.0, 500, 1500, "92 / AA add parameters assets"], "isController": false}, {"data": [1.0, 500, 1500, "82 / AA logout"], "isController": false}, {"data": [1.0, 500, 1500, "74 / SR reports"], "isController": false}, {"data": [0.0, 500, 1500, "AA Login"], "isController": true}, {"data": [1.0, 500, 1500, "SR Open Risk Management"], "isController": true}, {"data": [1.0, 500, 1500, "90 / AA click asset man"], "isController": false}, {"data": [1.0, 500, 1500, "SR Logout"], "isController": true}, {"data": [0.0, 500, 1500, "AA add asset"], "isController": true}, {"data": [1.0, 500, 1500, "SR Submit Risk with File Attachement"], "isController": true}, {"data": [1.0, 500, 1500, "AA click add asset tab on left"], "isController": true}, {"data": [1.0, 500, 1500, "AA 74 /reports"], "isController": false}, {"data": [1.0, 500, 1500, "83 /AA index.php"], "isController": false}, {"data": [1.0, 500, 1500, "SR Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "AA populate asset and click add"], "isController": true}, {"data": [1.0, 500, 1500, "91 / AA click add asset"], "isController": false}, {"data": [0.0, 500, 1500, "SR Login"], "isController": true}, {"data": [1.0, 500, 1500, "AA Logout"], "isController": true}, {"data": [0.0, 500, 1500, "73 AA / login asset man"], "isController": false}, {"data": [1.0, 500, 1500, "AA Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "77 SR submit risk with file"], "isController": false}, {"data": [1.0, 500, 1500, "83 /index.php"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 150, 0, 0.0, 907.3266666666661, 19, 6806, 6383.900000000001, 6514.4, 6748.370000000001, 0.35433266167608796, 1.15537823092332, 0.36810226882156044], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["65/ AA go to login page", 10, 0, 0.0, 39.300000000000004, 19, 194, 177.40000000000006, 194.0, 194.0, 0.026039361098236093, 0.034708128870100045, 0.00899680269194915], "isController": false}, {"data": ["AA Click Asset Management", 10, 0, 0.0, 45.7, 20, 206, 190.60000000000005, 206.0, 206.0, 0.02605184316790413, 0.05808746906343624, 0.011346798879770742], "isController": true}, {"data": ["Submit Risk", 10, 0, 0.0, 6827.900000000001, 6595, 7211, 7189.5, 7211.0, 7211.0, 0.025694654997867345, 0.6909553690266351, 0.26413051455473735], "isController": true}, {"data": ["82 / SR  logout", 10, 0, 0.0, 57.6, 53, 63, 62.9, 63.0, 63.0, 0.026287012113055182, 0.05000179901739149, 0.021625174808630552], "isController": false}, {"data": ["65/ SR go to login page", 10, 0, 0.0, 40.89999999999999, 19, 200, 182.80000000000007, 200.0, 200.0, 0.026173830880409147, 0.03506373164232937, 0.008874564532888728], "isController": false}, {"data": ["76 SR open risk management", 10, 0, 0.0, 51.3, 29, 231, 211.70000000000007, 231.0, 231.0, 0.026263814766567216, 0.17687550721992268, 0.011080046854645543], "isController": false}, {"data": ["73/ SR login", 10, 0, 0.0, 6495.1, 6318, 6693, 6690.8, 6693.0, 6693.0, 0.02574194727534359, 0.1373624495136059, 0.03554349536387529], "isController": false}, {"data": ["92 / AA add parameters assets", 10, 0, 0.0, 80.7, 53, 230, 216.50000000000006, 230.0, 230.0, 0.025954812671139547, 0.08469024958796734, 0.017337003776425244], "isController": false}, {"data": ["82 / AA logout", 10, 0, 0.0, 63.39999999999999, 51, 95, 93.30000000000001, 95.0, 95.0, 0.025955216868814548, 0.0490208881096556, 0.021494163969487046], "isController": false}, {"data": ["74 / SR reports", 10, 0, 0.0, 33.0, 29, 51, 49.300000000000004, 51.0, 51.0, 0.026241484638234894, 0.08999496655522782, 0.021090568219987612], "isController": false}, {"data": ["AA Login", 10, 0, 0.0, 6500.2, 6237, 6834, 6818.8, 6834.0, 6834.0, 0.02558565565801189, 0.22423928248866556, 0.05593863856167678], "isController": true}, {"data": ["SR Open Risk Management", 10, 0, 0.0, 51.3, 29, 231, 211.70000000000007, 231.0, 231.0, 0.02624306854951936, 0.1767357903624955, 0.011071294544328479], "isController": true}, {"data": ["90 / AA click asset man", 10, 0, 0.0, 45.7, 20, 206, 190.60000000000005, 206.0, 206.0, 0.02594471181911347, 0.057848599634179564, 0.011300138155590436], "isController": false}, {"data": ["SR Logout", 10, 0, 0.0, 80.4, 73, 85, 84.9, 85.0, 85.0, 0.026285422893026215, 0.08502974934877865, 0.032348525322062144], "isController": true}, {"data": ["AA add asset", 10, 0, 0.0, 6782.0, 6565, 7069, 7053.9, 7069.0, 7069.0, 0.025723538838685115, 0.566425291447695, 0.13642016603258142], "isController": true}, {"data": ["SR Submit Risk with File Attachement", 10, 0, 0.0, 127.2, 104, 173, 171.20000000000002, 173.0, 173.0, 0.02625705658395694, 0.1789762004398057, 0.1602603551266903], "isController": true}, {"data": ["AA click add asset tab on left", 10, 0, 0.0, 29.0, 25, 35, 35.0, 35.0, 35.0, 0.02594397139936593, 0.08323859730027033, 0.011299815668083206], "isController": true}, {"data": ["AA 74 /reports", 10, 0, 0.0, 34.400000000000006, 28, 50, 49.0, 50.0, 50.0, 0.026051978908317876, 0.0893399698578604, 0.020938260392134384], "isController": false}, {"data": ["83 /AA index.php", 10, 0, 0.0, 23.7, 20, 30, 29.700000000000003, 30.0, 30.0, 0.025807117603034918, 0.03439604893029497, 0.010433736999664506], "isController": false}, {"data": ["SR Open Login Page", 10, 0, 0.0, 41.0, 19, 200, 182.80000000000007, 200.0, 200.0, 0.02616287435802847, 0.03504905375424166, 0.008870849587019029], "isController": true}, {"data": ["AA populate asset and click add", 10, 0, 0.0, 80.7, 53, 230, 216.50000000000006, 230.0, 230.0, 0.025978344452064758, 0.08476703351336326, 0.017352722270715132], "isController": true}, {"data": ["91 / AA click add asset", 10, 0, 0.0, 29.0, 25, 35, 35.0, 35.0, 35.0, 0.02598043673114145, 0.08335559261376184, 0.011315698029383875], "isController": false}, {"data": ["SR Login", 10, 0, 0.0, 6528.099999999999, 6347, 6724, 6723.8, 6724.0, 6724.0, 0.025735190041510862, 0.22558502520761867, 0.056217818949077784], "isController": true}, {"data": ["AA Logout", 10, 0, 0.0, 87.1, 71, 121, 119.4, 121.0, 121.0, 0.025953936952696353, 0.08361020236284641, 0.03198619964287383], "isController": true}, {"data": ["73 AA / login asset man", 10, 0, 0.0, 6465.8, 6207, 6806, 6790.6, 6806.0, 6806.0, 0.025587488741504953, 0.13650825292464996, 0.035377701718967494], "isController": false}, {"data": ["AA Open Login Page", 10, 0, 0.0, 39.300000000000004, 19, 194, 177.40000000000006, 194.0, 194.0, 0.026198516639987842, 0.03492026890812442, 0.00905179217502705], "isController": true}, {"data": ["77 SR submit risk with file", 10, 0, 0.0, 127.2, 104, 173, 171.20000000000002, 173.0, 173.0, 0.026278860758828383, 0.17912482442436156, 0.1603934372487084], "isController": false}, {"data": ["83 /index.php", 10, 0, 0.0, 22.8, 19, 28, 28.0, 28.0, 28.0, 0.026198242097955227, 0.03491478612410107, 0.010689087449732123], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 150, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
